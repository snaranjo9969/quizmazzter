<?php include 'functions/func.php'; ?>
<?php if(!isset($_SESSION['user_id'])): ?>
	<?php $_SESSION['unauthorized'] = "Por Favor Ingrese Correo Electronico y Contraseña"; ?>
	<?php header("location:../index.php"); ?>
	<?php endif; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>FELICITACIONES SIGUE TRATANDO DE MEJORAR</title>
    <link rel="stylesheet" href="../game/style.css" />
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	  <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">
	  <link rel="stylesheet" href="../assets/css/style.css?202005281720">
  </head>
  <body>
  <?php include '../parts/nav.php'; ?>
    <div class="form-control">
      <div id="end" class="flex-center2 flex-column2">
        <h4>PUNTAJE ALCANZADO</h4>
        <h4 id="finalScore"> </h4>
        <a class="btn2" href="../game/game.php">JUGAR NUEVAMENTE</a>
        <a class="btn2" href="../profile/index.php">VOLVER A TU PERFIL</a>
      </div>
    </div>
    <script src="end.js"></script>
  </body>
</html>
